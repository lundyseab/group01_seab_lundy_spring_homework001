package com.example.group01_seab_lundy_spring_homework001;

import java.time.LocalDateTime;

public class DeleteResponse {
    private String message;
    private String status;
    private LocalDateTime time;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public DeleteResponse(String message, String status, LocalDateTime dateTime) {
        this.message = message;
        this.status = status;
        this.time = dateTime;
    }
}
