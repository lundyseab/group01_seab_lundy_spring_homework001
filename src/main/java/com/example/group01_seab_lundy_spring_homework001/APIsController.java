package com.example.group01_seab_lundy_spring_homework001;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@OpenAPIDefinition(info = @Info(title = "Customer REST API", version = "1.0"))
@RequestMapping("/api/v1/customers")
public class APIsController {
    List<Customer> customers = new ArrayList<>();
    public APIsController() {
        customers.add(new Customer(1, "Lundy Seab","Male", 21, "Prey Veng" ));
        customers.add(new Customer(2, "Johnny","Male", 23, "PP" ));
        customers.add(new Customer(3, "Vathany","Female", 22, "PP" ));
    }
    @PostMapping
    ResponseEntity<?> insertCustomer(@RequestBody CustomerRequest customer){
        try{
            int indexOfLastObject =-1;
            int id = 1;
            if (customers.size() > 0){
                indexOfLastObject = (customers.size() -1);
                id = customers.get(indexOfLastObject).getId() +1;
            }
            this.customers.add(new Customer(id, customer.getName(), customer.getGender(), customer.getAge(), customer.getAddress()));
            return ResponseEntity.ok(new CustomerResponse<>(
                    "This record was successfully created",
                    customers.get(indexOfLastObject +1),
                    "OK",
                    LocalDateTime.now()

            ));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    ResponseEntity<?> getAllCustomer (){
        return ResponseEntity.ok(new CustomerResponse<>(
                "You get all customer successfully",
                customers,
                "OK",
                LocalDateTime.now()

        ));
    }
    @GetMapping("/{customerId}")
    ResponseEntity<?> getCustomerByID(@PathVariable ("customerId") Integer id){
        Customer c = null;
            for (Customer i: customers) {
                if (i.getId() == id){
                    c = i;
                }
            }
            if (c != null){
                return ResponseEntity.ok(new CustomerResponse<>(
                        "This record has found successfully",
                        c,
                        "OK",
                        LocalDateTime.now()
                ));
            }
            else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/search")
    ResponseEntity<?> getCustomerByName(@RequestParam String name) {
        List<Customer> cus = customers.stream().filter(c -> c.getName().equals(name)).collect(Collectors.toList());
        if (!cus.isEmpty()){
            return ResponseEntity.ok(new CustomerResponse<>(
                    "This record has found successfully",
                    cus,
                    "OK",
                    LocalDateTime.now()
            ));
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{customerId}")
    ResponseEntity<?> updateCustomerById(@PathVariable ("customerId") Integer id, @RequestBody CustomerRequest customer){
        try {
            Customer customer1 = null;
            for (int i=0; i < customers.size(); i++){
                if (customers.get(i).getId() == id){
                    customers.set(i, new Customer(id, customer.getName(), customer.getGender(), customer.getAge(), customer.getAddress()));
                    customer1 = customers.get(i);
                    break;
                }
            }
            if (customer1 != null){
                return ResponseEntity.ok(new CustomerResponse<>(
                        "You're update successfully",
                        customer1,
                        "OK",
                        LocalDateTime.now()
                ));
            }else {
                return ResponseEntity.notFound().build();
            }

        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{customerId}")
    ResponseEntity<?> deleteCustomerByID(@PathVariable ("customerId") Integer id){
        Customer c = null;
        for (int i =0; i < customers.size(); i++){
            if (customers.get(i).getId() == id){
                c = customers.get(i);
                customers.remove(i);
                break;
            }
        }
        if (c != null){
            return ResponseEntity.ok(new DeleteResponse(
                    "Congratulation your delete is successfully",
                    "OK",
                    LocalDateTime.now()
            ));
        }else {
            return ResponseEntity.notFound().build();
        }
    }
}
