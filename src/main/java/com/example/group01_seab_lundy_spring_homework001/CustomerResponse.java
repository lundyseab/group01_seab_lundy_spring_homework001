package com.example.group01_seab_lundy_spring_homework001;

import java.time.LocalDateTime;

public class CustomerResponse<T> {
    private String message;
    private T customer;
    private String status;
    private LocalDateTime time;


    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }

    public CustomerResponse(String message, T customer, String status, LocalDateTime dateTime) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.time = dateTime;
    }
}
