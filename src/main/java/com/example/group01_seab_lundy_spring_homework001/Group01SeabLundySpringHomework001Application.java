package com.example.group01_seab_lundy_spring_homework001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Group01SeabLundySpringHomework001Application {

    public static void main(String[] args) {
        SpringApplication.run(Group01SeabLundySpringHomework001Application.class, args);
    }

}
